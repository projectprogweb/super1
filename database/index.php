<!DOCTYPE html>
<html>
<head>
	<title>Showcase UKDW</title>
	<link href="index.css" rel="stylesheet">
	<link rel = "stylesheet" type="text/css" href="css/index.css">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
	<div id="header">
		<h1>SHOWCASE UKDW</h1>
	<div id="navigation">
		<ul>
			<li><a href="http://vhost.ti.ukdw.ac.id/~gn15c2/index.html">Beranda</a></li>
			<li><a href="http://vhost.ti.ukdw.ac.id/~gn15c2/isiGaleri.html">Galeri</a></li>
			<li><a href="http://vhost.ti.ukdw.ac.id/~gn15c2/berita.html">Berita</a></li>
			<li><a href="http://vhost.ti.ukdw.ac.id/~gn15c2/AboutUs.html">Tentang Kami</a></li>
			<li><a href="http://vhost.ti.ukdw.ac.id/~gn15c2/AboutUkdw.html">Tentang UKDW</a></li>
			<li><a style="text-decoration:none; float: right;" href="login.php">Login</a></li>
			<li><a style="text-decoration:none; float: right;">Sign Up</a></li>
	</div>
	<div id="content">
		<p>"Talk is cheap. Show me the Code" - Linus Torvalds</p><br>
		<p>"Give a man a program, frustrate him for a day. Teach a man to program, frustrate him for a lifetime." - Waseem Latif</p><br>
		<p>"I'm not a great programmer, I'm just a good programmer with great habits." - Kent Beck</p><br>
		<p>"The most disastrous thing that you can ever learn is your first programming language." - Alan Kay</p>
	</div>
	<div id="footer">
		<h4>&copy Tim Project ProgWeb <br> <a href="http://vhost.ti.ukdw.ac.id/~gn15c2/contactus.html">Contact Us</a></h4>
	</div>
</body>
</html>