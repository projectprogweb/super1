<!DOCTYPE html>
<html>
<head>
    <title>Halaman Login</title>
    <link href="login.css" rel="stylesheet">
    <link rel = "stylesheet" type="text/css" href="login.css">
    <script type="text/javascript" src="javascript.js"></script>
</head>
<body>
    <body>
    <div id="header">
        <h1>SHOWCASE UKDW</h1>
    <div id="navigation">
        <ul>
            <li><a href="http://vhost.ti.ukdw.ac.id/~gn15c2/index.html">Beranda</a></li>
            <li><a href="http://vhost.ti.ukdw.ac.id/~gn15c2/isiGaleri.html">Galeri</a></li>
            <li><a href="http://vhost.ti.ukdw.ac.id/~gn15c2/berita.html">Berita</a></li>
            <li><a href="http://vhost.ti.ukdw.ac.id/~gn15c2/AboutUs.html">Tentang Kami</a></li>
            <li><a href="http://vhost.ti.ukdw.ac.id/~gn15c2/AboutUkdw.html">Tentang UKDW</a></li>
        </ul>
    </div>
    <br></br><br></br><br></br>
    <div id="login">
        <form action='cek.php' method='POST'>
            <p><input id="username" type="text" name="username" placeholder="Username"></p>
            <p><input id="password" type="password" name="password" placeholder="Password"></p>
            <p class="remember"><input type="checkbox" name="Remember me">Remember me</p>
            <input type="submit" value="Log in" id="submit"/>
            <br>
            </br>
        </form>    
    </div>
    <br></br><br></br><br></br>
    <div id="footer">
        <h4>&copy Tim Project ProgWeb <br> <a href="http://vhost.ti.ukdw.ac.id/~gn15c2/contactus.html">Contact Us</a></h4>
    </div>
</body>

</html>