<?php 
session_start();
if(isset($_SESSION["username"]) && $_SESSION["username"] != ""){
  header("Location: index.php");
  }
else
?>
<!DOCTYPE html>
<html>
<head>
    <title>Halaman Login</title>
    <link href="index.css" rel="stylesheet">
    <link rel ="stylesheet" type="text/css" href="css/index.css">
    <!--<script type="text/javascript" src="javascript.js"></script> -->
</head>
<body>
    <body>
    <div id="header">
        <h1>SHOWCASE UKDW</h1>
    <div id="navigation">
        <ul>
            <li><a href="index.php">Beranda</a></li>
            <li><a href="isiGaleri.php">Galeri</a></li>
            <li><a href="berita.php">Berita</a></li>
            <li><a href="AboutUs.php">Tentang Kami</a></li>
            <li><a href="AboutUkdw.php">Tentang UKDW</a></li>
        </ul>
    </div>
    <br></br><br></br><br></br>
    <div id="login">
        <form action='cek1.php' method='POST'>
            <p><input id="username" type="text" name="username" placeholder="Username"></p>
            <p><input id="password" type="Password" name="password" placeholder="Password"></p>
            <p class="remember"><input type="checkbox" name="Remember me">Remember me</p>
            <input  type="submit" value="Log in" id="submit">
                <br></br>
        </form>    
    </div>
    <br></br><br></br><br></br>
    <div id="footer">
        <h4>&copy Tim Project ProgWeb <br> <a href="contactus.php">Contact Us</a></h4>
    </div>
</body>

</html>